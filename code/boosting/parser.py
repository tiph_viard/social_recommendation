'''
Parsing the ACM Recsys Challenge 2017 data into interactions,
items and user models.

by Daniel Kohlsdorf and Tiphaine Viard
'''

from model import * 

def is_header(line):
    return "ml." in line 


def process_header(header):
    x = {}
    pos = 0
    for name in header:
        x[name] = pos
        pos += 1
    return x


def select(from_file, where, toObject, index):    
    header = None
    data = {}
    i = 0
    sep = ","
    for line in open(from_file):
        if is_header(line):
            header = process_header(line.strip().split(sep))
        else:
            cmp = line.strip().split(sep)
            if where(cmp):
                obj = toObject(cmp, header)
                if obj != None:
                    data[index(cmp)] = obj
        i += 1
        if i % 100000 == 0:
            print("... reading line " + str(i) + " from file " + from_file)
    return(header, data)        


def build_user(str_user, names):
    return User(
        int(str_user[names['ml.userId']]),        
        [
            float(str_user[names['ml.gdeg']]),        
            float(str_user[names['ml.avg']]),        
            float(str_user[names['ml.max']]),
            float(str_user[names['ml.min_itc']]),
            float(str_user[names['ml.max_itc']]),
            float(str_user[names['ml.mean_itc']]),
            float(str_user[names['ml.std_itc']]),
            float(str_user[names['ml.balanced']]),
            float(str_user[names['ml.avgDuration']]),
            float(str_user[names['ml.fractionOfCliques']]),
            float(str_user[names['ratingMean']]),
            float(str_user[names['ratingStd']]),
            float(str_user[names['ratingMedian']]),
            # float(str_user[names['nbRatings']]), # laissé un s dans le nom
            float(str_user[names['minRating']]),
            float(str_user[names['maxRating']]),
            float(str_user[names['nbRatingNorm']]), 
        ]
    )

def build_item(str_item, names):
    return Item(
        int(str_item[names['ml.movieId']]),
        [
            float(str_item[names['ml.gdeg']]),        
            float(str_item[names['ml.avg']]),
            float(str_item[names['ml.max']]),
            float(str_item[names['ml.min_itc']]),
            float(str_item[names['ml.max_itc']]),
            float(str_item[names['ml.mean_itc']]),
            float(str_item[names['ml.std_itc']]),
            int(str_item[names['Action']]),
            int(str_item[names['Adventure']]),
            int(str_item[names['Animation']]),
            int(str_item[names['Children']]),
            int(str_item[names['Comedy']]),
            int(str_item[names['Crime']]),
            int(str_item[names['Documentary']]),
            int(str_item[names['Drama']]),
            int(str_item[names['Fantasy']]),
            int(str_item[names['Film-Noir']]),
            int(str_item[names['Horror']]),
            int(str_item[names['Musical']]),
            int(str_item[names['Mystery']]),
            int(str_item[names['Romance']]),
            int(str_item[names['Sci-Fi']]),
            int(str_item[names['Thriller']]),
            int(str_item[names['War']]),
            int(str_item[names['Western']]),
            int(str_item[names['(no genres listed)']]),
            float(str_item[names['ml.balanced']]),
            float(str_item[names['ml.avgDuration']]),
            float(str_item[names['ml.fractionOfCliques']]),
            int(str_item[names['0']]),
            int(str_item[names['1890']]),
            int(str_item[names['1900']]),
            int(str_item[names['1910']]),
            int(str_item[names['1920']]),
            int(str_item[names['1930']]),
            int(str_item[names['1940']]),
            int(str_item[names['1950']]),
            int(str_item[names['1960']]),
            int(str_item[names['1970']]),
            int(str_item[names['1980']]),
            int(str_item[names['1990']]),
            int(str_item[names['2000']]),
            int(str_item[names['2010']]),
            float(str_item[names['ratingMean']]),
            float(str_item[names['ratingStd']]),
            float(str_item[names['ratingMedian']]),
            # float(str_item[names['nbRating']]),
            float(str_item[names['minRating']]),
            float(str_item[names['maxRating']]),
            float(str_user[names['nbRatingNorm']]), 

        ]
    )

class InteractionBuilder:
    
    def __init__(self, user_dict, item_dict):
        self.user_dict = user_dict
        self.item_dict = item_dict
    
    def build_interaction(self, str_inter, names):

        try:
            int(str_inter[names['ml.userId']]) in self.user_dict
        except KeyError:
            print(str_inter)
            print(names)


        if int(str_inter[names['ml.userId']]) in self.user_dict and int(str_inter[names['ml.movieId']]) in self.item_dict:
            return Interaction(
                self.user_dict[int(str_inter[names['ml.userId']])],
                self.item_dict[int(str_inter[names['ml.movieId']])],
                float(str_inter[names['ml.rating']]),
                [
                    list(str_inter[names['ml.tags']].split(" ")),
                    list(str_inter[names['ml.timestamps']].split(" ")),
                    float(str_inter[names['ml.assortativity']])
                ]
            )
        else:
            return None


