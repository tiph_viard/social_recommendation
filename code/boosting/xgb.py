'''
Baseline solution for the ACM Recsys Challenge 2017
using XGBoost

by Daniel Kohlsdorf and Tiphaine Viard
'''

import xgboost as xgb
import numpy as np
import os.path
import multiprocessing
import sys
import matplotlib.pyplot as plt
from scipy.sparse import csr_matrix as smatrix
import pandas as pd
from bayes_opt import BayesianOptimization
from tqdm import tqdm

DATA = sys.argv[1]
RESULTS = sys.argv[2]
C = ""

if "--content" in sys.argv:
    C = "content-"
DATA = DATA + C

user_feature_names = ['user.'+ x for x in open(DATA + "users.csv").readline().split(",")][1:]
item_feature_names = ['item.'+ x for x in open(DATA + "items.csv").readline().split(",")][1:]
stream_feature_names = ['stream.'+ x for x in open(DATA + "stream.csv").readline().split(",")][-1:]

feature_names = user_feature_names + item_feature_names + stream_feature_names
# print(user_feature_names)
# print(len(item_feature_names))
# print(len(stream_feature_names))
# print(len(feature_names))
# print(feature_names)

'''
Load data from libsvm formatted files
'''
xgtrain = xgb.DMatrix(DATA + "train.dat", feature_names=feature_names)
print("Done loading data.")
# In [1]: from sklearn.datasets import load_svmlight_file

# In [2]: mat = load_svmlight_file("./data/toy-movielens/train.dat")

# In [3]: X, y = mat

# In [4]: import pandas as pd

# In [5]: df = pd.SparseDataFrame(X,y, columns=feature_names)

# In [6]: df["target"] = y
# Continue on that !


'''
    Test BO
'''
def xgb_evaluate(min_child_weight,
                 colsample_bytree,
                 max_depth,
                 subsample,
                 gamma,
                 alpha):

    params['min_child_weight'] = int(min_child_weight)
    params['cosample_bytree'] = max(min(colsample_bytree, 1), 0)
    params['max_depth'] = int(max_depth)
    params['subsample'] = max(min(subsample, 1), 0)
    params['gamma'] = max(gamma, 0)
    params['alpha'] = max(alpha, 0)


    cv_result = xgb.cv(params, xgtrain, num_boost_round=num_rounds, nfold=5,
             seed=random_state,
             callbacks=[xgb.callback.early_stop(5)])

    return -cv_result['test-rmse-mean'].values[-1]

# num_rounds = 3000
# random_state = 123
# num_iter = 25
# init_points = 5
# params = {
    # 'eta': 0.1,
    # 'silent': 1,
    # 'eval_metric': 'rmse',
    # 'seed': random_state
# }

# xgbBO = BayesianOptimization(xgb_evaluate, {'min_child_weight': (1, 20),
                                            # 'colsample_bytree': (0.5, 1),
                                            # 'max_depth': (5, 11),
                                            # 'subsample': (0.5, 1),
                                            # 'gamma': (0, 10),
                                            # 'alpha': (0, 10),
                                            # })

# xgbBO.maximize(init_points=init_points, n_iter=num_iter, acq="ucb", kappa=1.0)
# ipdb.set_trace()


'''
3) Train XGBoost regression model
'''
evallist            = [(xgtrain, 'train')]
metrics             = ['ndcg@10', 'mae', 'rmse']

# all, toy
param = {
        'alpha': 0.3649,
        'eta': 0.1,
        'min_child_weight': 18.6967,
        'colsampleby_tree': 0.9112,
        'gamma':0.9930,
        'silent': 1,
        'max_depth': 10,
        'subsample': 0.9810, 
        'objective': 'reg:linear',
        'eval_metric': metrics
        }
# content, toy
# Step |   Time |      Value |     alpha |   colsample_bytree |     gamma |   max_depth |   min_child_weight |   subsample |
# 28    | 00m53s |   -0.86643 |    9.3515 |             0.9541 |    2.7449 |     10.6648 |            11.5504 |      0.9959 | 
# param = {
        # 'alpha': 9.3515,
        # 'eta': 0.1,
        # 'min_child_weight': 11.5504,
        # 'colsampleby_tree': 0.9541,
        # 'gamma': 2.7449,
        # 'silent': 1,
        # 'max_depth': 10,
        # 'subsample': 0.9959, 
        # 'objective': 'reg:linear',
        # 'eval_metric': metrics
        # }
# ls, toy
# Step |   Time |      Value |     alpha |   colsample_bytree |     gamma |   max_depth |   min_child_weight |   subsample |
#    2 | 01m17s |   -0.89979 |    4.3687 |             0.7021 |    0.6916 |     10.3025 |             2.7966 |      0.5923 |
# param = {
        # 'alpha': 0.3649,
        # 'eta': 0.1,
        # 'min_child_weight': 18.6967,
        # 'colsampleby_tree': 0.9112,
        # 'gamma':0.9930,
        # 'silent': 1,
        # 'max_depth': 10,
        # 'subsample': 0.9810, 
        # 'objective': 'reg:linear',
        # 'eval_metric': metrics
        # }


num_round           = 15 #30000

# bst = xgb.train(param, dataset, num_round, evallist) 
# if hasattr(bst, 'feature_names'): bst.set_attr(feature_names = '|'.join(bst.feature_names))
# bst.save_model("model2")

cv_res = xgb.cv(param, xgtrain, num_round, early_stopping_rounds=50, nfold=5, seed=123, verbose_eval=True)

cv_file_name = "cv-results.csv"
# if "toy" in DATA:
    # cv_file_name += "toy-"
# if "content-based" in DATA:
    # cv_file_name += "cb-"
# else:
    # cv_file_name += "ls-"
# cv_file_name += str(num_round) + ".csv"

cv_res.to_csv(RESULTS + "/" + C + cv_file_name)

# xgb.plot_importance(bst)
# plt.show()
# y_pred = bst.predict(test_dm)
# print(mean_squared_error(test_dm.get_label(), y_pred))
# print(auc(test_dm.get_label(), y_pred, reorder=True))
# print(ndcg_score(test_dm.get_label(), y_pred, k=5))
# average_precision = average_precision_score(test_dm.get_label(), y_pred)

# print('Average precision-recall score: {0:0.2f}'.format(
      # average_precision))


# precision, recall, _ = precision_recall_curve(test_dm.get_label(), y_pred)

# plt.step(recall, precision, color='b', alpha=0.2,
         # where='post')
# plt.fill_between(recall, precision, step='post', alpha=0.2,
                 # color='b')

# plt.xlabel('Recall')
# plt.ylabel('Precision')
# plt.ylim([0.0, 1.05])
# plt.xlim([0.0, 1.0])
# plt.title('2-class Precision-Recall curve: AP={0:0.2f}'.format(average_precision))
# plt.show()
# report = classification_report(test_dm.get_label(), y_pred)
# print(report)

