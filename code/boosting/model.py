'''
Modeling users, interactions and items from
the MovieLens dataset.

by Daniel Kohlsdorf and Tiphaine Viard
'''
import random

class User:

    def __init__(self, user, features):
        self.userId = user
        self.features = features


class Item:

    def __init__(self, movie, features):
        self.movieId = movie
        self.features = features


class Interaction:
    
    def __init__(self, user, item, rating, features):

        self._features = features

        self.user = user
        self.item = item
        self.rating = float(rating)

    def myfeatures(self):
        return [len(x) for x in self._features if type(x) == list] + [x for x in self._features if not type(x) == list]

    def features(self):
        return self.user.features + self.item.features + self.myfeatures()

    def label(self):
        return self.rating
        if self.rating >= 4:
            return 1
        # elif self.rating <= 3:
            # return -1
        else:
            return 0
