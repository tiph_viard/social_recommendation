import time
import numpy
from sklearn.feature_extraction import DictVectorizer #figure this out
from sklearn.preprocessing import normalize
from pyfm import pylibfm
import pandas as pd
from scipy import sparse
from random import randint
from sklearn.metrics import mean_squared_error

# Create Dicts

iter = 1

userData    = {}
movieData   = {}
x_Data      = []
y_Data      = []
x_Data_C    = []
x_Data_S    = []
x_Train     = []
y_Train     = []
x_Train_C   = []
x_Train_S   = []


DATA        = "./data/movielens"
USERS       = DATA + "/users.csv"
MOVIES      = DATA + "/items.csv"
STREAM      = DATA + "/stream.csv"

userFt      = (open(USERS).readlines()[0]).split(',')
movieFt     = (open(MOVIES).readlines()[0]).split(',')
streamFt    = (open(STREAM).readlines()[0]).split(',')[4:]  #check with tiph! should it be 3 or 4

userFtContent   = userFt[0:3]
movieFtContent  = movieFt[0:-1]

userFtStream    = userFt[4:]
movieFtStream   = movieFt[-1:]

for i in range(len(userFt)):
    userFt[i] = userFt[i] + '.user'

userData    = {int(line.split(",")[0]): [float(x) for x in line.split(",")[0:] ] for line in open(USERS).readlines()[1:]}
movieData   = {int(line.split(",")[0]): [float(x) for x in line.split(",")[0:] ] for line in open(MOVIES).readlines()[1:]}

i = 0
for line in open(STREAM):
        
    if "ml." in line:
        continue
        
    if (i<99000):
        u = int(line.split(",")[0])
        m = int(line.split(",")[1])
        r = float(line.split(",")[2])
        streamData = [float(x) for x in (line.split(",")[4:])]

        y_Data.append(r)
        userData[u][0] =  str(userData[u][0]) 
        movieData[m][0] = str(movieData[m][0])
        x_Data.append(dict(zip(userFt+movieFt+streamFt,userData[u]+movieData[m]+streamData)))
        x_Data_C.append(dict(zip(userFtContent+movieFtContent,userData[u][0:3]+movieData[m][0:-1])))
        x_Data_S.append(dict(zip(userFtStream+movieFtStream+streamFt,userData[u][4:]+movieData[m][-1:]+streamData)))
        
    else:
        u = int(line.split(",")[0])
        m = int(line.split(",")[1])
        r = float(line.split(",")[2])
        streamData = [float(x) for x in (line.split(",")[4:])]

        y_Train.append(r)
        userData[u][0] =  str(userData[u][0]) 
        movieData[m][0] = str(movieData[m][0])
        x_Train.append(dict(zip(userFt+movieFt+streamFt,userData[u]+movieData[m]+streamData)))
        x_Train_C.append(dict(zip(userFtContent+movieFtContent,userData[u][0:3]+movieData[m][0:-1])))
        x_Train_S.append(dict(zip(userFtStream+movieFtStream+streamFt,userData[u][4:]+movieData[m][-1:]+streamData)))
    i+=1

v = DictVectorizer()
x = v.fit_transform(x_Data)
y = numpy.asarray(y_Data)
x = x / numpy.linalg.norm(x)

xTest = v.fit_transform(x_Train)
yTest = numpy.asarray(y_Train)
xTest = normalize(sparse.csr_matrix(xTest))

info = {}
res = []

#Run Randomized Search
for i in range(iter):
    num_fact = randint(2,10)
    num_iters = randint(5,10)
    fm = pylibfm.FM(num_factors=num_fact, num_iter = num_iters, verbose=False, task='regression')
    print('------ ', i, ' ------')
    print(num_fact, num_iters)
    fm.fit(x,y)
    preds = fm.predict(xTest)
    mse = mean_squared_error(yTest,preds)
    info[mse] = [num_fact, num_iters]
    res.append(mse)

res.sort()

def print_results(res_list, top_rank):
    for i in range(top_rank):
        print(res[i], info[res[i]][0], info[res[i]][1])

