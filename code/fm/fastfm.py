from fastFM import sgd
from sklearn.metrics import mean_squared_error
import scipy.sparse  as sparse
import numpy
from sklearn.feature_extraction import DictVectorizer
from sklearn.preprocessing import normalize
from sklearn.model_selection import train_test_split


userData    = {}
movieData   = {}
x_Data      = []
y_Data      = []
x_Data_C    = []
x_Data_S    = []
x_Train     = []
y_Train     = []
x_Train_C   = []
x_Train_S   = []


DATA        = "./toy-data/movielens"
USERS       = DATA + "/users.csv"
MOVIES      = DATA + "/movies.csv"
STREAM      = DATA + "/stream.csv"

userFt      = (open(USERS).readlines()[0]).split(',')
movieFt     = (open(MOVIES).readlines()[0]).split(',')
streamFt    = (open(STREAM).readlines()[0]).split(',')[4:]  #check with tiph! should it be 3 or 4

userFtContent   = userFt[0:3]
movieFtContent  = movieFt[0:-1]

userFtStream    = userFt[4:]
movieFtStream   = movieFt[-1:]

for i in range(len(userFt)):
    userFt[i] = userFt[i] + '.user'

userData    = {int(line.split(",")[0]): [float(x) for x in line.split(",")[0:] ] for line in open(USERS).readlines()[1:]}
movieData   = {int(line.split(",")[0]): [float(x) for x in line.split(",")[0:] ] for line in open(MOVIES).readlines()[1:]}

i = 0
for line in open(STREAM):
        
    if "ml." in line:
        continue
        
    u = int(line.split(",")[0])
    m = int(line.split(",")[1])
    r = float(line.split(",")[2])
    streamData = [float(x) for x in (line.split(",")[4:])]

    y_Data.append(r)
    userData[u][0] =  str(userData[u][0]) 
    movieData[m][0] = str(movieData[m][0])
    x_Data.append(dict(zip(userFt+movieFt+streamFt,userData[u]+movieData[m]+streamData)))
    x_Data_C.append(dict(zip(userFtContent+movieFtContent,userData[u][0:3]+movieData[m][0:-1])))
    x_Data_S.append(dict(zip(userFtStream+movieFtStream+streamFt,userData[u][4:]+movieData[m][-1:]+streamData)))
    
    
v = DictVectorizer()
x = v.fit_transform(x_Data)
y = numpy.asarray(y_Data)
x = normalize(x)

x_train, x_test, y_train, y_test = train_test_split(x, y, test_size=0.10, random_state=42)

fm = sgd.FMRegression(n_iter=1000, init_stdev=0.1, rank=500000, l2_reg_w=0.1, l2_reg_V=0.1, step_size=0.01)
fm.fit(x_train,y_train)

y_pred = fm.predict(x_test)

seaborn.set(style="whitegrid")
seaborn.violinplot(x=y_test, y=y_pred)
mse = mean_squared_error(y_pred,y_test)
print(mse)
plt.xlabel("test")
plt.ylabel("pred")
plt.show()

