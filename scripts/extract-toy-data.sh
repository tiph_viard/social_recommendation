#!/bin/bash

if [[ $# -ne 2 ]]; then
    echo "Usage: ./extract-toy-data.sh <num_lines> <data_directory>"
    exit 2
fi

cat $2/users.csv > ./toy-$2/users.csv
cat $2/movies.csv > ./toy-$2/movies.csv
head -n $1 $2/stream.csv > ./toy-$2/stream.csv
