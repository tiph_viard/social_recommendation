# ignore rating; transform each b e u v link into b 1 u, b 1 v, e -1 u, e -1 v before using 
import sys
import pandas as pd

degree = 0
t_old = 0
t_start = 0
# t_end = 638131998 
t_end = 0
d_max = 0
all_time_max = 0
u_old = -1
cpt = 1
columns = ["ml.avg", "ml.max"]

try:
    nodeListFile = open(sys.argv[1])
except:
    print("Error")
    sys.exit(1)

allNodes = [ i.strip() for i in nodeListFile.readlines() ]

degrees = { i: [0 for i in range(0, len(columns))] for i in allNodes}

for line in sys.stdin:
    contents = line.split(",")
    t = int(contents[0])
    arr_type = int(contents[1])
    u = contents[2].strip()
    
    if u_old == -1:
        # degrees.loc[u] = [0 for i in range(0, len(degrees.columns))]
        degrees[u] = [0 for i in range(0, len(columns))]

    if u != u_old and u_old != -1:
        # print(",".join(map(str, [u_old, degree, t_old, t_end])))
        # print(cpt)
        cpt += 1
        degree = 0
        d_max = 0
        t_old = 0
        # degrees.loc[u] = [0 for i in range(0, len(degrees.columns))]
        degrees[u] = [0 for i in range(0, len(columns))]
    
    # print(",".join(map(str, [u, degree, t_old, t])))
    if t > t_end:
        t_end = t
    
    if degree > d_max:
        # degrees.loc[u]["max"] = degree
        degrees[u][1] = degree
        d_max = degree
    if degree > all_time_max:
        all_time_max = degree
    # degrees.loc[u]["avg"] += degree * ((t - t_old) / (t_end - t_start) )
    degrees[u][0] += degree * (t - t_old)

    degree += arr_type
    t_old = t
    u_old = u

# print(" ".join(map(str, [u, degree, t_old, t_end])))
cols = [sys.argv[2]] + columns
sys.stdout.write(",".join(cols))
sys.stdout.write("\n")
for u in degrees:    
    sys.stdout.write("%s,%f,%f\n" % (u, degrees[u][0]/(t_end - t_start), degrees[u][1]/all_time_max))

