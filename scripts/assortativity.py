import pandas as pd
import sys
import ipdb

data_users = {int(x.split(",")[0]): float(x.split(",")[1]) for x in open("./data/users.csv").readlines()[1:] }
data_movies = {int(x.split(",")[0]): float(x.split(",")[1]) for x in open("./data/movies.csv").readlines()[1:] }

user_maxdeg = 15219
movie_maxdeg = 67329

print("ml.userId,ml.movieId,ml.assortativity")
for line in sys.stdin:
    contents = line.split(",")
    u = int(contents[0])
    m = int(contents[1])


    d_u = data_users[u] * user_maxdeg
    d_m = data_movies[m] * movie_maxdeg

    assortativity = min(d_u, d_m) / max(d_u, d_m)
    print(",".join(map(str, [u,m,assortativity])))
