import sys
import csv

features = [ "nbRatings", "minRating", "maxRating" ]

sys.stdout.write("%s,%s\n"%("ml.userId",",".join(features)))
userPrec = ""
max = 0
min = 1000
count = 0

for row in csv.reader(iter(sys.stdin.readline, '')):
    userId, movieId, rating, timestamp = row
    # RAZ and print
    if userId != userPrec:
        # sys.stdout.write("%s,%s\n"%(movieid,",".join(str(x) for x in vecteur)))
        # sys.stdout.write("%s,%s\n" % ("coucou","coucoud"))
        sys.stdout.write("%s,%s,%s,%s\n" % (userPrec, str(count), str(min), str(max)))
        max = 0
        min = 1000
        count = 0

    minmaxUser = []
    if float(rating) < min:
        min = float(rating)
        # print("--- change min", str(min))
    if float(rating) > max:
        max = float(rating)
    userPrec = userId
    count += 1

# print for last user
sys.stdout.write("%s,%s,%s,%s\n" % (userPrec, str(count), str(min), str(max)))
