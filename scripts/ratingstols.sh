#!/bin/bash

input=$1;
output=$2;

alpha=$(tail -n +2 $input/originaldata/ratings.csv | cut -d, -f4 |sort -T. -S4g -k1,1n|head -n1);
omega=$(tail -n +2 $input/originaldata/ratings.csv | cut -d, -f4 |sort -T. -S4g -k1,1rn|head -n1);

tail -n +2 $input/originaldata/ratings.csv| awk -F, -v alpha=$alpha -v omega=$omega '{
    b = $4 - 43200;
    e = $4 + 43200;
    u = $1;
    v = $2;

    if(b < alpha) b = alpha;
    if(e > omega) e = omega;

    print(b","e","u","v);
}' > $output/tmp.csv

cat $output/tmp.csv |sort -k3,3 -k4,4 -k1,1n -k2,2n -S 5G -T . -t, | awk -F, 'BEGIN{
        OFS = ",";
        old_b = -1;
        old_e = -1;
        old_u = -1;
        old_v = -1;
    }{
    
    b=$1; e=$2; u=$3; v=$4;
    
    if( (u!= old_u || v != old_v) && old_u != -1 && old_v != -1) {
        # New link
        print(old_b, old_e, old_u, old_v);
        old_b=-1;old_e = -1;
    }

    if(b < old_e) b = old_b;
    if(e > old_e && b < old_e) old_e=e;
    if(e < old_e) e = old_e;
    
    if(b > old_e){
        if(old_b != -1 && old_e != -1) print(old_b, old_e,old_u,old_v);
        old_b = b; old_e = e;
    }

    old_u = u; old_v = v;

}END{
    print(old_b, old_e,old_u,old_v);
}' > $input/linkstream.csv 
rm $output/tmp.csv
