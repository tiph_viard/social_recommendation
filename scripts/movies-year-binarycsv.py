import sys
import csv
import re

# sys.stdout.write("%s,%s\n"%("ml.movieId",",".join(genres)))
years = set()
years = [0, 1891, 1893, 1894, 1895, 1896, 1898, 1899, 1900, 1901, 1902, 1903,
          1905, 1909, 1910, 1912, 1913, 1914, 1915, 1916, 1917, 1918, 1919,
          1920, 1921, 1922, 1923, 1924, 1925, 1926, 1927, 1928, 1929, 1930,
          1931, 1932, 1933, 1934, 1935, 1936, 1937, 1938, 1939, 1940, 1941,
          1942, 1943, 1944, 1945, 1946, 1947, 1948, 1949, 1950, 1951, 1952,
          1953, 1954, 1955, 1956, 1957, 1958, 1959, 1960, 1961, 1962, 1963,
          1964, 1965, 1966, 1967, 1968, 1969, 1970, 1971, 1972, 1973, 1974,
          1975, 1976, 1977, 1978, 1979, 1980, 1981, 1982, 1983, 1984, 1985,
          1986, 1987, 1988, 1989, 1990, 1991, 1992, 1993, 1994, 1995, 1996,
          1997, 1998, 1999, 2000, 2001, 2002, 2003, 2004, 2005, 2006, 2007,
          2008, 2009, 2010, 2011, 2012, 2013, 2014, 2015]

sys.stdout.write("%s,%s\n"%("ml.movieId",",".join(str(x) for x in years)))

for row in csv.reader(iter(sys.stdin.readline, '')):
    movieid, title, genresbrut = row
    vecteur = [0]*len(years) # on cree un vect de 0, qui contiendra 1 pour l'annee du film
    m = re.findall('(\([0-9]+\))+', title)
    # print(title,m[-1])
    try:
        # year = int(m.group(0)[1:-1])
        year = int(m[-1][1:-1])
    except:
        year = 0
    vecteur[years.index(year)] = 1
    sys.stdout.write("%s,%s\n"%(movieid,",".join(str(x) for x in vecteur)))
