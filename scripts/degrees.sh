#!/bin/bash

# Compute graph degrees
#echo -n "Compute graph degrees..."
#echo "ml.userId,ml.gdeg" > ./results/graph-users-degrees.dat
#tail -n +2 ./data/linkstream.csv |awk -F, '{print($3)}'|sort -T. -S4g -k1,1n -t, |uniq -c|awk '{print($2","$1)}' >> ./results/graph-users-degrees.dat

#echo "ml.movieId,ml.gdeg" > ./results/graph-movies-degrees.dat
#tail -n +2 ./data/linkstream.csv |awk -F, '{print($4)}'|sort -T. -S4g -k1,1n -t, |uniq -c|awk '{print($2","$1)}' >> ./results/graph-movies-degrees.dat
#echo "Done."
# Compute pre-degree files
echo -n "Compute pre-degree files..."
tail -n +2 ./data/linkstream.csv|awk -F, '{
    print($1",1,"$3);
    print($2",-1,"$3);
}'|sort -T. -S4g -k3,3g -k1,1g -k2,2g -t, > ./results/pre-users-degrees.dat

tail -n +2 ./data/linkstream.csv|awk -F, '{
    print($1","1","$4);
    print($2",-1,"$4);
}'|sort -T. -S4g -k3,3g -k1,1g -k2,2g -t, > ./results/pre-movies-degrees.dat
echo "Done."
 Compute stream degree files
echo -n "Compute stream degrees..."
cat ./results/pre-users-degrees.dat|python3 ./scripts/degrees.py ml.userId 1>./results/degrees-users-overtime.dat 2>./results/summary-degrees-users.dat
cat ./results/pre-movies-degrees.dat|python3 ./scripts/degrees.py ml.movieId 1>./results/degrees-movies-overtime.dat 2>./results/summary-degrees-movies.dat
echo "Done."

# Assemble summary table
echo -n "Assemble summary table..."
join -t, -j 1 -o 1.1,1.2,2.2,2.3 ./results/graph-users-degrees.dat ./results/summary-degrees-users.dat > ./results/degree-info-users.dat
join -t, -j 1 -o 1.1,1.2,2.2,2.3 ./results/graph-movies-degrees.dat ./results/summary-degrees-movies.dat > ./results/degree-info-movies.dat
echo "Done."
echo ""
echo "Finished. Summary files are ./results/degree-info-users.dat and ./results/degree-info-movies.dat"
