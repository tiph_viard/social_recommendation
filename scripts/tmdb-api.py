import pandas as pd
import json
import grequests

base_url = "https://api.themoviedb.org/3/movie/"
api_key = "?api_key=72c53e6124fbdd9b0c07cde6fa110ee5"

movies = []
ids = pd.read_csv("./ml-20m/links.csv")
tmdb_ids = [i for i in ids['tmdbId'] if str(i) != "nan"]
urls = [base_url + str(int(x)) + api_key for x in tmdb_ids]
i = 0

rs = (grequests.get(u) for u in urls)
g = grequests.map(rs)
r = [ i.json() for i in g if i != None ]
open("api_results.json", "w").write(json.dumps(r))
