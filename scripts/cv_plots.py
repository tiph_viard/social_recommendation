# coding: utf-8
import pandas as pd
import matplotlib.pyplot as plt
import sys

cv = pd.read_csv(sys.argv[1])

for i in range(5,11):
    ax = plt.subplot(3, 6, i - 4)
    plt.plot(cv['train-ndcg@' + str(i) + '-mean'], label='NDCG@' + str(i) )
    plt.plot(cv['test-ndcg@' + str(i) + '-mean'], label='NDCG@' + str(i) )
    plt.title("NDCG@" + str(i))

for i in range(5,11):
    ax = plt.subplot(3, 6, 6 + (i - 4))
    plt.plot(cv['train-map@' + str(i) + '-mean'], label='MAP@' + str(i) )
    plt.plot(cv['test-map@' + str(i) + '-mean'], label='MAP@' + str(i) )
    plt.title("MAP@" + str(i))

# AUC
ax = plt.subplot(3, 6, 13)
plt.plot(cv['train-auc-mean'])
plt.plot(cv['test-auc-mean'])
plt.title("AUC")

# RMSE
ax = plt.subplot(3, 6, 14)
plt.plot(cv['train-rmse-mean'])
plt.plot(cv['test-rmse-mean'])
plt.title("RMSE")
    
plt.show()
