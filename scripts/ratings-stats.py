# Usage: python ./scripts/ratings-stats.py [--user|--item]
# Input: ratings.csv, sorted by user/item
import sys
import csv

features = [ "nbRating", "minRating", "maxRating", "meanRating", "medianRating"]

if "--user" in sys.argv:
    # sys.stdout = open("./results/users-ratings-stats.csv", "w")
    sys.stdout.write("%s,%s\n"%("ml.userId",",".join(features)))
elif "--item" in sys.argv:
    # sys.stdout = open("./results/items-ratings-stats.csv", "w")
    sys.stdout.write("%s,%s\n"%("ml.itemId",",".join(features)))

_id_prec = None
max_rating = 0
min_rating = 1000
nb_ratings = 0
mean_rating = 1.0
median_rating = 1.0

for row in csv.reader(iter(sys.stdin.readline, '')):
    _id, rating = row
    # RAZ and print
    if _id != _id_prec and _id_prec is not None:
        features = [ _id_prec, nb_ratings, min_rating, max_rating, mean_rating, median_rating]
        sys.stdout.write(",".join(map(str, features)) + "\n")

        max_rating = 0
        min_rating = 1000
        mean_rating = 1.0
        median_rating = 1.0
        nb_ratings = 0

    if float(rating) < min_rating:
        min_rating = float(rating)
    if float(rating) > max_rating:
        max_rating = float(rating)
    _id_prec = _id
    nb_ratings += 1

# Last
# sys.stdout.write("%s,%s,%s,%s\n" % (moviePrec, str(count), str(min), str(max)))
features = [_id_prec, nb_ratings, min_rating, max_rating, mean_rating, median_rating]
sys.stdout.write(",".join(map(str, features)) + "\n")
