import matplotlib.pyplot as plt
import xgboost as xgb
import ipdb
import sys
import operator

font = {'family' : 'normal',
        'size'   : 15}

plt.rc('font', **font)

bst = xgb.Booster()
bst.load_model("../model")
if bst.attr('feature_names') is not None: bst.feature_names = bst.attr('feature_names').split("|")
bst.feature_types = None

stream_features_names = [
        'user.ml.gdeg', 'user.ml.avg', 'user.ml.max', 'user.ml.min_itc', 'user.ml.max_itc', 'user.ml.mean_itc', 'user.ml.std_itc', 'user.ml.balanced', 'user.ml.avgDuration', 'user.ml.fractionOfCliques', 'item.ml.gdeg', 'item.ml.avg', 'item.ml.max', 'item.ml.min_itc', 'item.ml.max_itc', 'item.ml.mean_itc', 'item.ml.std_itc', 'item.ml.balanced', 'item.ml.avgDuration', 'item.ml.fractionOfCliques'
        ]
print("Total stream features: " + str(len(stream_features_names)))
sorted_features = sorted(bst.get_fscore().items(), key=operator.itemgetter(1), reverse=True)

plt.figure(figsize=(12,9))

x = [i for i in range(0, len(sorted_features))]
y = [i[1] for i in sorted_features]
plt.bar(x, y)
# plt.xticks(x, [i[0] for i in sorted_features], rotation="45", ha='right')
plt.xticks(x, [i[0] for i in sorted_features], rotation="vertical")
ticklabels = plt.gca().get_xticklabels()
plt.ylabel("F-score")

for tick in ticklabels:
    if tick.get_text() in stream_features_names:
        tick.set_color('red')
plt.tight_layout()
plt.savefig("../tex/img/feature_importance_toy.eps")
plt.show()
