import sys
import csv

genres = [
    "Action",
    "Adventure",
    "Animation",
    "Children",
    "Comedy",
    "Crime",
    "Documentary",
    "Drama",
    "Fantasy",
    "Film-Noir",
    "Horror",
    "Musical",
    "Mystery",
    "Romance",
    "Sci-Fi",
    "Thriller",
    "War",
    "Western",
    "(no genres listed)",
]

sys.stdout.write("%s,%s\n"%("ml.itemId",",".join(genres)))

for row in csv.reader(iter(sys.stdin.readline, '')):
    movieid, title, genresbrut = row
    genresmovie = genresbrut.split("|")
    vecteur = [0]*19
    for g in genresmovie:
        try:
            vecteur[genres.index(g)] = 1
        except:
            pass
    sys.stdout.write("%s,%s\n"%(movieid,",".join(str(x) for x in vecteur)))
