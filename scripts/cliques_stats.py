from pandas import DataFrame, read_csv
import sys
import math

IN = sys.argv[1]
OUT = sys.argv[2]
NODE_INDEX  = IN + "/cliques/node_index"
T = float(sys.argv[4]) - float(sys.argv[3]) #638131998.0 
total_cliques = 0

df = read_csv(NODE_INDEX, sep=" ")

# index_movies = { int(x.new_id): x.old_id for (_,x) in df[df.side == 'm'].sort_values('old_id').iterrows() }
# index_users = { int(x.new_id): x.old_id for (_,x) in df[df.side == 'u'].sort_values('old_id').iterrows() }
index_movies = { x.new_id: x.old_id for (_,x) in df[df.side == 'm'].astype(str).sort_values('old_id').iterrows() }
index_users = { x.new_id: x.old_id for (_,x) in df[df.side == 'u'].astype(str).sort_values('old_id').iterrows() }

users_stat = { x: (0, 0.0, 0.0) for x in index_users }
movies_stat = { x: (0, 0.0, 0.0) for x in index_movies }

for line in sys.stdin:
    total_cliques += 1
    contents = line.strip().split(" ")

    b = float(contents[0])
    e = float(contents[1])
    users = set()
    movies = set()

    nodes = [ x.strip() for x in contents[3:] ]

    for x in nodes:
        if x in index_users:
            users.add(x)
        elif x in index_movies:
            movies.add(x)
        else:
            print("Err.")
            sys.exit(1)

    if len(users) == 0 and len(movies) == 0:
        print(line)
        print(users)
        print(movies)
        continue

    # clique_stat = ( len(users) * len(movies) ) / math.floor(0.5*(len(users) + len(movies)) ** 2)
    clique_stat = min(len(users), len(movies)) / max(len(users), len(movies))
    
    for u in users:
        users_stat[u] = (users_stat[u][0] + 1, users_stat[u][1] + clique_stat, users_stat[u][2] + ((e - b)/T))
    for m in movies:
        movies_stat[m] = (movies_stat[m][0] + 1, movies_stat[m][1] + clique_stat, movies_stat[m][2] + ((e - b)/T))

fm = open(OUT + "/clique_items.csv", "w")
fu = open(OUT + "/clique_users.csv", "w")

print("ml.itemId,ml.balanced,ml.avgDuration,ml.fractionOfCliques", file=fm)
for m in index_movies:
    print("%s,%f,%f,%f" % (index_movies[m], movies_stat[m][1] / max(1, movies_stat[m][0]), movies_stat[m][2] / max(1, movies_stat[m][0]), movies_stat[m][0]/float(total_cliques)), file=fm)

print("ml.userId,ml.balanced,ml.avgDuration,ml.fractionOfCliques", file=fu)
for u in index_users:
    print("%s,%f,%f,%f" % (index_users[u], users_stat[u][1] / max(1, users_stat[u][0]), users_stat[u][2] / max(1, users_stat[u][0]), users_stat[u][0] / float(total_cliques)), file=fu)


fm.close()
fu.close()
