import ipdb
res = "train-mae:0.62902+0.000186846   train-ndcg@10:0.992833+0.0143344        train-rmse:0.820867+0.00019876  test-mae:0.639003+0.000173519 test-ndcg@10:0.971283+0.0242068 test-rmse:0.834994+0.00036392"
res = "train-mae:0.619163+0.000265127  train-ndcg@10:0.992833+0.0143344 train-rmse:0.807977+0.000321165 test-mae:0.635256+0.000155082 test-ndcg@10:0.971283+0.0242068 test-rmse:0.830591+0.00034968" 
res2 = [i for i in res.split(" ") if i != '']
dic = {x.split(":")[0]:x.split(":")[1] for x in res2}

out_order = ['test-mae', 'test-ndcg@10', 'test-rmse', 'train-mae', 'train-ndcg@10', 'train-rmse']
str_head = ""
str_res = ""

for x in out_order:
    str_head += "," + x + '-mean,' + x + '-std'
    y = dic[x].split("+")
    mean = y[0]
    std = y[1]
    str_res += "," + mean + "," + std
print(str_head)
print(str_res)
