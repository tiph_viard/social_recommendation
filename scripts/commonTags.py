import sys

data = sys.argv[1]

allUsers = [u.strip() for u in open(data + "/allUsersIds.csv")]
allItems = [i.strip() for i in open(data + "/allUsersIds.csv")]

user_tags = {u: set() for u in allUsers}
item_tags = {i: set() for i in allItems}

for line in open(data + "/originaldata/tags.csv"):
    if "userId" in line:
        continue
    u = line.split(",")[0].strip()
    i = line.split(",")[1].strip()
    tag = line.split(",")[2].strip()

    user_tags[u].add(tag)
    item_tags[i].add(tag)
# tags per user, per item ?

print("userId,itemId,commonTags")
for line in open(data + "/originaldata/ratings.csv"):
    if "userId" in line:
        continue

    u = line.split(",")[0].strip()
    i = line.split(",")[1].strip()
    
    if len(user_tags[u]) > 0 and len(item_tags[i]) > 0:
        J_ui = float(len(user_tags[u].intersection(item_tags[i])) / len(user_tags[u].union(item_tags[i])))
    else:
        J_ui = 0.0
    print(u,i,J_ui,sep=",")
