tail -n+2 ../../ml-20m/movies.csv | python movies-genres-binarycsv.py > ../data/movies-genres-binary.csv
tail -n+2 ../../ml-20m/movies.csv | python movies-year-binarycsv.py > ../data/movies-year-binarycsv.csv
# debugging mal formed years
for m in `cat movies-year-binarycsv.notAccountedMovies`; do grep $m ../../ml-20m/movies.csv; done

# pour merge les resultats
python IOHelper.py ../data/movies.csv ../data/movies.csv ../data/movies-year-binarycsv.csv
