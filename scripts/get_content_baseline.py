import pandas as pd

IN = "./data"
OUT = "./content-based"
USERS = IN + "/users.csv"
MOVIES = IN + "/movies.csv"
STREAM = IN + "/stream.csv"


ls_users_features = [
'ml.gdeg',
'ml.avg',
'ml.max',
'ml.min_itc',
'ml.max_itc',
'ml.mean_itc',
'ml.std_itc',
'ml.balanced',
'ml.avgDuration',
'ml.fractionOfCliques'
]

ls_movies_features = [
        'ml.gdeg',
'ml.avg',
'ml.max',
'ml.min_itc',
'ml.max_itc',
'ml.mean_itc',
'ml.std_itc',
'ml.balanced',
'ml.avgDuration',
'ml.fractionOfCliques'
]

ls_stream_features = [
    'ml.timestamps',
    'ml.assortativity'
]

def write_content_based_df(df, ls_features, outfile):
    df_content = df[ [i for i in df.columns if i not in ls_features] ]
    df_content.to_csv(outfile, index=False)

df_users = pd.read_csv(USERS, delimiter=",")
write_content_based_df(df_users, ls_users_features, OUT + "/users.csv")

df_movies = pd.read_csv(MOVIES, delimiter=",")
write_content_based_df(df_movies, ls_movies_features, OUT + "/movies.csv")

df_stream = pd.read_csv(STREAM, delimiter=",")
write_content_based_df(df_stream, ls_stream_features, OUT + "/stream.csv")
