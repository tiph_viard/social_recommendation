# Input: List of all nodes, list of nodes appearance (on stdin)
import sys


# Read all nodes
try:
    nodeListFile = open(sys.argv[1])
except:
    print("Error")
    sys.exit(1)

allNodes = [ i.strip() for i in nodeListFile.readlines() ]

degrees = { i: 0 for i in allNodes}

for line in sys.stdin:
    nodeId = line.strip()
    
    try:
        degrees[nodeId] += 1
    except KeyError:
        print("Node does not exist.")
        sys.exit(2)
for u in degrees:
    print("%s,%d" % (u, degrees[u]))
