import pandas as pd
import re
import sys

if len(sys.argv) != 2:
    print("Usage: ./script.py <filename>")
    sys.exit(1)

def getYear(title):
    m = re.findall('\(([1,2][0-9]{3})-{0,1}\)', title)

    try:
        return int(m[-1])
    except IndexError:
        return 0

def getDecade(x):
    x = int(x)
    
    if x <= 1940:
        return 1940
    else:
        return int(str(x)[:-1] + '0')

df = pd.read_csv(sys.argv[1], delimiter=",")

# Extract years and get decade
df['ml.year'] = df['title'].apply(getYear)
df['ml.decade'] = df['ml.year'].apply(getDecade)
# Decade histogram
# import matplotlib.pyplot as plt
# df["ml.decade"].value_counts(normalize=True).plot(kind="bar")
# plt.savefig("decades.png")

out = pd.concat([df.movieId, pd.get_dummies(df['ml.decade'])], axis=1 )
out = out.rename(index=str, columns={"movieId": "ml.itemId"})
out.to_csv(sys.stdout, index=False)
# out.to_csv("./results/items-decades.csv", index=False)
