import sys
import numpy as np

old_u = -1
old_t = 0
itc = []
duration = int(sys.argv[3]) - int(sys.argv[2]) # omega - alpha

# min_itc is always 0, remove
columns = [sys.argv[1], "max_itc","mean_itc","std_itc"]

print(",".join(columns))
for line in sys.stdin:
    (u, t) = [x.strip() for x in line.split(",")]
    t = int(t)

    if u != old_u and old_u != -1:
        old_t = t
        if itc == []:
            itc = [0]
        print("%s,%f,%f,%f" \
                % (old_u, max(itc)/duration, np.mean(itc)/duration, np.std(itc)/duration))
        itc = []
    else:
        itc.append(t - old_t)
    old_u = u
    old_t = t
if itc == []:
    itc = [0]
print("%s,%f,%f,%f" \
        % (old_u, max(itc)/duration, np.mean(itc)/duration, np.std(itc)/duration))
