# Includes
include: "snakefiles/processing.sf"
include: "snakefiles/user_features.sf"
include: "snakefiles/item_features.sf"


# Except to get data, a configuration file is necessary
if not "--configfile" in sys.argv:
    print("Please provide a configuration file with the --configfile option.")
    sys.exit(2)

rule all:
    input:
        expand("{results}/cv-results.csv", results=config["results"]),
        expand("{results}/content-cv-results.csv", results=config["results"])

rule boosting:
    input:
        expand("{data}/train.dat", data=config["data"])
    output:
        expand("{results}/cv-results.csv", results=config["results"])
    shell:
        "python3 code/boosting/xgb.py {config[data]} {config[results]}"

rule content_boosting:
    input:
        expand("{data}/content-train.dat", data=config["data"])
    output:
        expand("{results}/content-cv-results.csv", results=config["results"])
    shell:
        "python3 code/boosting/xgb.py {config[data]} {config[results]} --content"

rule convert_libsvm:
    input:
        expand("{data}/users.csv", data=config["data"]),
        expand("{data}/items.csv", data=config["data"]),
        expand("{data}/content-users.csv", data=config["data"]),
        expand("{data}/content-items.csv", data=config["data"])
    output:
        expand("{data}/train.dat", data=config["data"]),
        expand("{data}/content-train.dat", data=config["data"])
    shell:
        """
            python3 utils/prepare_train_data.py {config[data]}
            python3 utils/prepare_train_data.py {config[data]} --content
        """

rule features:
    input:
        expand("{results}/content-users.csv", results=config["results"]),
        expand("{results}/content-items.csv", results=config["results"]),
        expand("{results}/users.csv", results=config["results"]),
        expand("{results}/items.csv", results=config["results"])
    output:
        expand("{data}/users.csv", data=config["data"]),
        expand("{data}/items.csv", data=config["data"]),
        expand("{data}/content-users.csv", data=config["data"]),
        expand("{data}/content-items.csv", data=config["data"])
    shell:
        """
            cp {config[results]}/users.csv {config[data]}users.csv
            cp {config[results]}/items.csv {config[data]}items.csv
            cp {config[results]}/stream.csv {config[data]}stream.csv
            cp {config[results]}/content-users.csv {config[data]}content-users.csv
            cp {config[results]}/content-items.csv {config[data]}content-items.csv
            cp {config[results]}/content-stream.csv {config[data]}content-stream.csv
        """

rule content_features:
    input:
        u=expand("{results}/users-ratings-stats.csv", results=config["results"]),
        i=expand([
            "{results}/items-ratings-stats.csv", 
            "{results}/items-decades.csv", 
            "{results}/items-genres.csv", 
        ], results=config["results"]),
        s=expand([
            "{data}/originaldata/ratings.csv",
            #"{results}/commonTags.csv"
            ],
            results=config["results"], data=config["data"]
        )
    output:
        u=expand("{results}/content-users.csv", results=config["results"]),
        i=expand("{results}/content-items.csv", results=config["results"]),
        s=expand("{results}/content-stream.csv", results=config["results"])
    shell:
        """
            python3 utils/IOHelper.py 1 {input.u} > {output.u}
            python3 utils/IOHelper.py 1 {input.i} > {output.i}
            python3 utils/IOHelper.py 2 {input.s} > {output.s}
        """

rule all_features:
    input:
        u=expand([
            "{results}/users-ratings-stats.csv", 
            "{results}/graph-users-degrees.dat", 
            "{results}/summary-degrees-users.dat", 
            "{results}/itc-users.csv", 
            "{results}/clique_users.csv", 
        ], results=config["results"]),
        i=expand([
            "{results}/items-ratings-stats.csv", 
            "{results}/items-decades.csv", 
            "{results}/items-genres.csv", 
            "{results}/graph-items-degrees.dat", 
            "{results}/summary-degrees-items.dat", 
            "{results}/itc-items.csv", 
            "{results}/clique_items.csv",
        ], results=config["results"]),
        s=expand([
            "{data}/originaldata/ratings.csv",
            #"{results}/commonTags.csv"
            ],
            results=config["results"], data=config["data"]
        )
    output:
        u=expand("{results}/users.csv", results=config["results"]),
        i=expand("{results}/items.csv", results=config["results"]),
        s=expand("{results}/stream.csv", results=config["results"])
    shell:
        """
            python3 utils/IOHelper.py 1 {input.u} > {output.u}
            python3 utils/IOHelper.py 1 {input.i} > {output.i}
            python3 utils/IOHelper.py 2 {input.s} > {output.s}
        """

rule common_tags:
    input:
        expand("{data}/originaldata/ratings.csv", data=config["data"]),
        tagfile=expand("{data}/originaldata/tags.csv", data=config["data"])
    output:
        expand("{results}/commonTags.csv", results=config["results"])
    shell:
        "python3 scripts/commonTags.py {config[data]} > {output}"

rule cliques:
    input: expand("{data}/cliques/maxcliques.gz", data=config["data"])
    output:
        expand("{results}/clique_items.csv", results=config["results"]),
        expand("{results}/clique_users.csv", results=config["results"]),
    shell:
        """
            zcat {input} | python3 ./scripts/cliques_stats.py {config[data]} {config[results]} {config[alpha]} {config[omega]}
            cat {config[results]}/clique_items.csv | python3 utils/padNodes.py {config[data]}/allItemsIds.csv > tmp_output
            mv tmp_output {config[results]}/clique_items.csv
            cat {config[results]}/clique_users.csv | python3 utils/padNodes.py {config[data]}/allUsersIds.csv > tmp_output
            mv tmp_output {config[results]}/clique_users.csv
        """

rule prepare_linkstream:
    input:
        expand("{data}/originaldata/ratings.csv", data=config["data"])
    output:
        expand("{data}/linkstream.csv", data=config["data"])
    shell:
        "./scripts/ratingstols.sh {config[data]} {config[results]}"

rule prepare_items_degrees:
    input:
        expand("{data}/linkstream.csv", data=config["data"])
    output:
        expand("{results}/pre-items-degrees.dat", results=config["results"])
    shell:
        """
            tail -n +2 {input} |awk -F, '{{
                print($1","1","$4);
                print($2",-1,"$4);
            }}'|sort -T. -S4g -k3,3g -k1,1g -k2,2g -t, > {output}
        """

rule extract_toy_data:
    input: ""
    output: ""
    shell: ""


