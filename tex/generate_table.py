import sys
import pandas as pd

def format_value(val):
    mu = val[0][0]
    sigma = val[0][1]
    isbest = val[1]
    f_val = round_float(mu, 4) + " ($\pm$ " + round_float(sigma, 1) + ")"
    
    if isbest:
        f_val = "{\\bf" + f_val + "}"

    return f_val

def score(val):
    return val[0]

round_float = lambda x,y: str(float(('%.' + str(y) + 'e') % x))
min_metrics = ["mae", "rmse"]

if len(sys.argv) != 3:
    sys.exit()

c = pd.read_csv(sys.argv[1], delimiter=",").columns[1:]
cv_res_cb = pd.read_csv(sys.argv[1], delimiter=",").iloc[-1]
cv_res_ls = pd.read_csv(sys.argv[2], delimiter=",").iloc[-1]
metrics = list(set([ i.split("-")[1] for i in c ]))
vals = {"cb": {"nrounds": 0}, "ls": {"nrounds": 0}}

print("\\begin{tabular}{|c|c|c|c|c|}")
print("\\hline")
print("\\multirow{2}{*}{Metric} & \\multicolumn{2}{c|}{With stream features} & \\multicolumn{2}{c|}{Without stream features}\\\\")
# print("\\hline")
print("& train & test & train & test\\\\")
print("\\hline")

for m in metrics:
    vals["cb"]["train"] = (cv_res_cb['train-' + m + '-mean'],cv_res_cb['train-' + m + '-std'])
    vals["cb"]["test"] = (cv_res_cb['test-' + m + '-mean'],cv_res_cb['test-' + m + '-std'])
    vals["ls"]["train"] = (cv_res_ls['train-' + m + '-mean'],cv_res_ls['train-' + m + '-std'])
    vals["ls"]["test"] = (cv_res_ls['test-' + m + '-mean'],cv_res_ls['test-' + m + '-std'])

    f = []
    if m in min_metrics:
        compare = lambda x,y: x < y
    else:
        compare = lambda x,y: x >= y


    if compare(score(vals["cb"]["train"]), score(vals["ls"]["train"])):
        vals["cb"]["train"] = (vals["cb"]["train"], True)
        vals["ls"]["train"] = (vals["ls"]["train"], False)
    else:
        vals["cb"]["train"] = (vals["cb"]["train"], False)
        vals["ls"]["train"] = (vals["ls"]["train"], True)
    if compare(score(vals["cb"]["test"]), score(vals["ls"]["test"])):
        vals["cb"]["test"] = (vals["cb"]["test"], True)
        vals["ls"]["test"] = (vals["ls"]["test"], False)
    else:
        vals["cb"]["test"] = (vals["cb"]["test"], False)
        vals["ls"]["test"] = (vals["ls"]["test"], True)
     
    f = [vals[i][j] for i in ["cb", "ls"] for j in ["train", "test"]]
    
    f1 = " & ".join([m.upper()] +  list(map(format_value, f)))
    print(f1 + "\\\\")
    print("\\hline")
print("\end{tabular}")
