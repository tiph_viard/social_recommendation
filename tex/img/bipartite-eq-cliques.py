from Drawing import *

b = Drawing()

b.addNode("u")
b.addNode("x")
b.addNode("v")
b.addNode("y")

b.addLink("u","y", 1, 2, height=0.40)
b.addLink("u","x", 2, 7)
b.addLink("u","x", 9, 10)
b.addLink("v","x", 3, 8)
b.addLink("v","y", 4, 5)

# b.addRectangle("u","v", 1, 10, color=11)
# b.addRectangle("u","y", 4, 5, color=12)

b.addTimeLine(ticks=1)
