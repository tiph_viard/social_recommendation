\documentclass[12pt]{article}

\usepackage{amssymb}
\usepackage{libertine}
\input{./comnet-packages}
\input{../packages}
\input{../newcommands}
\usepackage[top=1.5cm,bottom=1.5cm,left=1.5cm,right=1.5cm]{geometry}
\graphicspath{{../}}

\usepackage{ifthen}
\usepackage[shortlabels]{enumitem}
\usepackage[dvipsnames]{xcolor}


\definecolor{authorGreen}{cmyk}{0, 0.8, 1, 0}

\newcommand{\authitem}[1]{%
\begin{itemize}
\item[\textcolor{ForestGreen}{\Large\textbullet}]{\textcolor{ForestGreen}{\textbf{#1}\\}}
\end{itemize}
}
\newcommand{\revitem}[1]{%
  \item[\textcolor{red}{\Large\textbullet}]{#1}
}

\title{Reply to reviewers' comments \\
  for the paper -- originally -- titled \\
  {\bf Movie rating prediction using content-based\\
  and link stream features}
}
\author{Tiphaine Viard, Raphaël Fournier-S'niehotta}

\begin{document}

\setlist[enumerate,1]{leftmargin=9mm}

\maketitle

Dear Editor, \\

We warmly thank the reviewers for their positive feedback and valuable comments
and suggestions. We did our best to follow their recommendations, which led to
significant improvement on the presentation and quality of our paper. We even
obtain some slightly better performance with their suggestions, which we are
grateful for. We detail below the changes we have made to the paper. We
reproduce the comments of our reviewers (issues starts with a
\textcolor{red}{\Large\textbullet}) and we answer in \textcolor{ForestGreen}{\bf
bold green paragraphs starting with a \Large\textbullet}.

We shall note that we included more datasets along the suggestion from Reviewer
2, which led us to change the title of the paper for a more appropriate one.
Since the submission, we have collaborated closely with Anna Deza on our
experiments and we have included her as a co-author.

\section*{Reviewer 1}

The paper investigates incorporating features of link streams into
recommendation system in order to take into account temporal user-item
relations. To that end, authors explore the notion of a bipartite link stream.
As a proof-of-concept study, authors apply the investigated methodology in the
particular case of movie rating prediction using a large-scale dataset from
that area of application and compare results for the link streams based
methodology with a baseline that considers only content-based features. The
paper is in general well written, clearly motivating the tackled problem and
investigation solution. 

There are, however, some issues in the paper that require addressing or better
explanation:

\begin{itemize}

  \revitem{At the end of the introduction, while discussing the performed
  evaluation study, add some brief discussion of the achieved results.}

  \authitem{We have added a discussion of our results at the end of the
  introduction.}

  \revitem{At the end of Section 2, in the sentence "The contents of each of
    these sets is discussed below", use a direct cross reference to clearly
    indicate in which part of the paper this is discussed in further detail,
    e.g. "… discussed in Section X"}

  \authitem{We added a reference to the appropriate section.}

  \revitem{At the last paragraph of Section 3, it is said that the general
  properties of graphs have been generalized to link streams. Where?}

  \authitem{We added the correct reference to Latapy et al. (2017).}

  \revitem{In the second paragraph of Section 4.1, I understand not using IMDB
    identifiers as this would require complementary NLP work that could be seen
    as out of scope of this particular application.  However, the same design
    decision should not apply to user-generated tags??? Not interpreting those
    tags as positive or negative, but just for connectivity counting between
    users and items, could not distort the results to some extent? How important
    this connectivity counting is to the shown results? Just not taking such
  connectivity counting significantly impacts the results??? Please elaborate on
this issue.} 

  \authitem{We followed the reviewer's suggestion which is very relevant. We
    devised tag-based features and obtain slightly better results on the
    MovieLens dataset. We thank the reviewer for this input. We did similar work
    on the Goodread dataset, which leads to good results too.
}

  \revitem{In Section 5.2, please elaborate on the sentence that the results
  outperform those of the recent literature, referencing [6]. Bring the results
  from the literature for a clearer comparison and to make the paper more
  self-contained. Is [6] a direct alternative to compare the investigated
approach with?}

\authitem{We modified the paragraph to include more context on the cited
reference and the comparison of our results with theirs.}

  \revitem{Figure 2 in Section 5.2 is actually Table 2, as it is referred along
  the text in the section. Please correct that paying attention that Figure 3
then becomes Figure 2.}

  \authitem{We modified the text and the references so that Figure 2 is actually
  a Table.}

  \revitem{In Section 6 (related work), while discussing time-varying graphs,
  the link streams abstraction is the only alternative that focuses on
  generalising graph-related concepts to dynamic networks??? Analyzing papers
  that in principle target similar goals or touch the same particular topic
  should better position the link streams contribution in general and its
  particular appropriateness to the investigated topic in the submitted paper.
  Papers such as Michail and Spirakis, Communications of the ACM, Feb 2018;
  Wehmuth et al, Theoretical Computer Science, Oct 2016; and Scholtes et al,
  European Physical Journal B, Mar 2016; as well as possible references therein
could be interesting to be discussed in this context.}

\authitem{We thank the reviewer for these references, which were very relevant
  to our topics, even if those models have not been used in a recommender system
  setting. We presented some of the models in our improved Related Work section, and
  included some content from these references. It will help the interested
  reader in understanding the position of the link stream model in the broader
topic of time-varying graphs models.}

  \revitem{In the last paragraph of Section 7, I suggest authors to complement
  the text being more specific about possible directions for future work. For
  instance, is community detection in link streams an open issue? If yes,
indicate so, and so on.}

\authitem{We had a sentence in the original submission explaining that community
  detection could improve link stream models and recommender systems in general.
  We modified the wording of this paragraph and developed the section to include
  a more precise roadmap, first dealing only with the extension of graph
  properties to link streams of other time-varying graphs, then with the Graph
Convolutional Networks approach, which is complementary and highly efficient.}

\end{itemize}

Minor points:

\begin{itemize}

  \revitem{page 3, line 50, lacks ==> lack;}

  \authitem{A better proof-reading of the whole article was done, to remove such
  obvious typos.}

  \revitem{page 3, line 53, the sentence indicating the shorthand notation is
  redundant with a similar sentence stated in the previous paragraph and may be
safely removed.}

  \authitem{The sentence was modified accordingly.}

  \revitem{Some references lack the year of publication (e.g. [12], [13], [15];
  and possibly others). I believe [18] is entitled "A Guide … " and not "A
Guidance"}

  \authitem{A wrong citation style was used in the submitted paper, causing some
    years to disappear for no obvious reason. We are sorry for this mistake and
    thank the reviewer for pointing it out. We made the required changes. Beware
    that the bibliography is now sorted alphabetically by first-author's name
  (before that it was in the order of citation in the text).}

\end{itemize}


\section*{Reviewer \#2}

The paper presents a method for predicting movie ratings by users leveraging
machine learning on features that concern both content and link streams. The
topic of user recommendation has received enormous attention in the past, and
the main novelty of the present work is to introduce new features from a
bipartite link-stream representation of the user-item interactions.

I found the paper to be an interesting reading. However, there are multiple weak
aspects that could be addressed to improve its quality and contribution, as
follows.

\begin{itemize}

  \revitem{The level of technical detail is shallow. For instance, the authors
  show that their proposed approach yields better performance than an equivalent
  method that only leverages content-based features (Fig.2), and that
  link-stream features are in fact important for the recommendation (Fig.3):
however, which link-stream features are actually relevant is not discussed;}

\authitem{We have improved several sections of the paper to up the level of
  technical detail significantly. We discussed more extensively and precisely
  the specific point on which LS features are relevant. See below for other points.}

\revitem{similarly, why such features are especially important is unclear (which
is very disappointing in the light of the authors' claim that link-stream
features have an "added bonus of justifiability"). }

\authitem{We have added a paragraph clarifying our aim, which may have been
  confusing in the submission. Our goal is not to make the algorithm more
  explainable but rather build upon the fact that graph and link streams are
  more easily interpretable than most matrix factorization or deep learning
techniques. In this light, the high ranking of link stream features in the plot
is a result in itself.}
  
  \revitem{Also, the results are far from the current state-of-the-art (see
  Sec.5.2), but there is no clear discussion on why it is so; }

  \authitem{We have now added some sentences to discuss this fact, in the
  introduction and the conclusion sections.}
  
\revitem{the perspectives in Sec.7 are vague and do not
offer factual evidences that the gap can be closed. }

  \authitem{ The purpose of our work is to demonstrate that link stream features
    offer valuable information which can be included into a classic recommender
    system scenario. The state of the art mostly relies on deep learning
    approaches, which we do not aim at replacing but completing. We have
  included a paragraph to present this avenue for future work, and precised our
perspectives. As stated in the new version of our text, we are confident that
current approaches lack graph-based and link stream/dynamic networks features. }

  \revitem{On the same note, more information on (1) whether the values in Fig.2
    and those attained by the state of the art (0.7652 RMSE) are actually "good"
  in absolute terms (e.g., can be leveraged for effective recommendation) [...]} 

  \authitem{For (1), the RMSE is a traditional metric which has been in use for
    years in the recommender system community, as a proxy for evaluating
    effective recommendation (see the highly-cited article by Herlocker et al.
    in 2004). We have added a sentence to clarify that in the section 5.1. The
    state of the art attains a good score, and it is credible that our results
  would also lead to good reco. However, articles presenting new elements for
recommender algorithms like this one tend to stick to including the RMSE metric,
not other quality-related information. }

  \revitem{ [...] and (2) XgBoost and its configuration (and if/how this matters
  in terms of performance) are needed to make the paper self-contained.}

\authitem{For (2), the paragraph presenting XGBoost parameters and their
importance for the recommending task was added. }

  \revitem{The author suggest in the abstract that link streams and tensor
  factorization can be seen as the two sides of a same object, yet there is no
clear discussion of this throughout the paper.}

  \authitem{We removed this claim, as it was no longer relevant in the new
  version of the paper.}

  \revitem{The authors experiment with one specific dataset. More tests with
  additional, heterogeneous datasets are needed in order to increase the
credibility of the approach, and generalize the results.}

  \authitem{We introduced a new dataset, Goodreads, provided publicly by a
    prominent member of the recommender system academic community, Julian
    McAuley. It is of reasonable size, comparable with the Movielens 20M. It
    brings heterogeneity, being a collection of reviews on books. It clearly
    adds some credibility and robustness to our results and approach.\\ We
    wanted to include additional datasets traditionally used in evaluating
    recommender systems but it was not possible due to the missing of usable
    content information (e.g., {\em Ciao, Epinions}), public inaccessibility
  (Flixster, Douban), or size issues (LastFM, Netflix). }

\end{itemize}

There are also a couple of minor issues:

\begin{itemize}

  \revitem{it is unclear to me why 14 features are needed to encode the *decade*
  of movie release: are there movies released in 1880, and how many? Does it
make sense to include movies released before 1940?}

  \authitem{ The reviewer is completely right on this. We observed the feature
  importance of those features, and it became obvious that several of them
should be merged, see figure~\ref{fig:decades} below. The code and the article
were modified to account for this. }

\begin{figure}[!h]
\centering
\includegraphics[width=.7\columnwidth]{decades.png}
\caption{Feature importance histogram, for the features encoding the decade of
the movie release. Separating the oldest decades seems of little interest and
such decades can be merged in a single feature.}
\label{fig:decades}
\end{figure}

  \revitem{the paper is well written overall, yet there are typos that should be
  corrected throughout the document (e.g., "are complex objects that currently
lacks", "which call for"); I suggest attentive proofreading.}

  \authitem{A better proof-reading of the whole article was done, to remove such
  obvious typos.}

\end{itemize}

We thank again the reviewers for their time and valuable comments, which we have
all addressed. 


\end{document}
