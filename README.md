# Getting started

Ensure beforehand that you have `snakemake`, `pip`, and a recent installation of `python`.

Activate the virtual environment:

```bash
python3 -m venv venv
. venv/bin/activate
```

Install required packages:

```bash
pip install -r requirements.txt
```

# Download toy data

First of all, download the "toy" datasets. These are shortened versions of the data, suited to run on a local computer in a reasonable time.
The file is about 900MB large. 

```bash
snakemake --configfile config-toy-movielens.json get_data
```

# Run 

To run on the MovieLens dataset:

```bash
snakemake --configfile config-toy-movielens.json clean
snakemake --configfile config-toy-movielens.json -j
```

To run on the goodreads children dataset:
```bash
snakemake --configfile config-toy-gr-children.json clean
snakemake --configfile config-toy-gr-children.json -j
```

# Cleanup

```bash
snakemake --configfile <config_file.json> clean
```
