# Merges features files into one CSV according to a common key
# It is the user's responsibility to ensure that:
#   - First column is the common key
#   - IDs are sorted in all files
# TODO: Throw error if IDs do not match

import pandas as pd
import sys


class IOHelper:
    def __init__(self, key=1):
        self.data = pd.DataFrame()
        self.headers = []
        self.exclude = []
        self.key = key

    def write(self, target):
        f = open(target, "w")
        f.write(self.data.to_csv()) 

    def load(self, str_files):
        raw_data = [] 
        index = []
        
        files = [ open(f) for f in str_files]
        is_header = True 
        for line in files[0]:
            # Ugly trick to read all files together
            lines = [ line ] + [ f.readline() for f in files[1:] ]
            
            try:
                if len(set([x.split(",")[0] for x in lines])) != 1:
                    raise ValueError
            except ValueError:
                sys.stderr.write("ValueError: Fields do not match. Ensure that all input files are sorted in the same way and have the same key name.\n")
                sys.exit(1)


            if is_header:
                # Read headers
                is_header = False
                index_name = lines[0].split(",")[0:self.key]
                [ self.headers.extend([ i.strip() for i in x.split(",")[self.key:] ]) for x in lines ]
                print(",".join(index_name + self.headers))
                cpt = 1
            else:
                cpt += 1
                # if cpt % 10000 == 0:
                    # sys.stderr.write("%d lines merged.\n" % (cpt))
                curr_node = lines[0].split(",")[0:self.key]
                index.append(curr_node)

                tmp = [ i for i in curr_node ]
                [ tmp.extend([ i.strip() for i in x.split(",")[self.key:] ]) for x in lines ]
                # print(self.headers)
                # print(tmp)
                assert(len(tmp) == len(self.headers) + self.key)

                print(",".join(tmp))
                # raw_data.append(tmp)

        self.data = pd.DataFrame(raw_data, index=index , columns=self.headers)
        self.data.index.name = index_name


if __name__ == '__main__':
    io = IOHelper(key=int(sys.argv[1]))
    io.load(sys.argv[2:])
    # io.write(sys.argv[1])
