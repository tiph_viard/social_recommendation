import sys
import numpy
import pandas as pd



DATA                =   sys.argv[1]
if "--content" in sys.argv:
    C = "content-"
else:
    C = ""
USERS               =   DATA + C + "users.csv"
MOVIES              =   DATA + C + "items.csv"
# STREAM              =   DATA + "originaldata/ratings.csv"
STREAM              =   DATA + C + "stream.csv"

train   = open(DATA + C + "train.dat", "w")

df_users = {line.split(",")[0].strip(): [float(x) for x in line.split(",")[1:] ] for line in open(USERS).readlines()[1:]}
df_movies = {line.split(",")[0].strip(): [float(x) for x in line.split(",")[1:] ] for line in open(MOVIES).readlines()[1:]}
df_stream = {(line.split(",")[0].strip(),line.split(",")[1].strip()): [float(x) for x in line.split(",")[-1:]] for line in open(STREAM).readlines()[1:]}

def label(rating):
    return rating

cpt = 1
for line in open(STREAM):
    if "userId" in line:
        continue
    if cpt % 100000 == 0:
        print("Parsed %d lines" % (cpt))
    cpt += 1

    u = line.split(",")[0].strip()
    m = line.split(",")[1].strip()
    r = float(line.split(",")[2])
    
    # print(df_users[u])
    # print(df_movies[m])
    # print(df_stream[(u,m)])

    # features =  numpy.append(
            # df_users[u],
            # df_movies[m],
            # )
    features = df_users[u] + df_movies[m] + df_stream[(u,m)]
    
    x = ((i, features[i]) for i in range(0, len(features)))
    y = label(r)

    print(str(y) + " " + " ".join([ str(i[0]) + ":" + str(i[1]) for i in x if i[1] != 0]), file=train)
