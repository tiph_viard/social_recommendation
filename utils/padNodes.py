import sys


try:
    nodeListFile = open(sys.argv[1])
except:
    print("Error")
    sys.exit(1)

allNodes = [ i.strip() for i in nodeListFile.readlines() ]

features = {}


for line in sys.stdin:
    if "ml." in line:
        # Headers
        columns = line.strip().split(",")
        features = { i: ",".join([str(i)] + [str(0) for i in range(0, len(columns) -1)]) for i in allNodes}
    else:
        nodeId = line.split(",")[0].strip()
        features[nodeId] = line.strip() 
print(",".join(columns))

for u in sorted(features):
    print(features[u])
